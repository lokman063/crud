<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use app\Models\Role;
class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    
    {
        $roles = Role::all();
        return view('role.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('role.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
  
        $valiDatedData = $request->validate([
            'role_name' => 'required|unique:roles|max:50'
        ]);
        
        $role = Role::create([
        'role_name' => $request->role_name
        ]);
    
        if($role){
            return redirect('role')->with('success','Data saved Successfully');
        }else{
            return redirect('role')->with('error', 'Data can not saved');


        }






        // $valiDatedData = $request->validate([
        //     'role_name ' => 'required|unique:roles|max:50'
        // ]);

       

        // $role = new Role();
        // $role->role_name = $request->role_name;
        // $role->save();

        // if (!empty($role)) {
        //     redirect(role.index);
        // }else{
        //     redirect(role.add);
        // }
      
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
