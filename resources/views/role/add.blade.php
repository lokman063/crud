@extends('master')

@section('content')


  <div class="container">
    <a href="{{url('role')}}" class="btn btn-danger btn-lg active" role="button" aria-pressed="true">Back</a>
    <div class="row justify-content-center">
        <div class="pt-3 col-md-6 ">
        <form action="{{url('role/store')}}" method="POST">
          @csrf

                <div class="row">
                  <div class="col">
                    <label for="role_name">Role Name</label>
                    <input type="text" class="form-control @error('role_name') is-invalid @enderror" name="role_name" id="role_name">
                    @error('role_name')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
               <div>
                <div class="pt-3 form-group">
                  <button type="submit" class="btn btn-secondary">Save</button>
                 </div>
               </div>
                </div>
               
              </form> 
    </div>
</div>


@endsection