@extends('master')

@section('content')
<section class="container-fluid">
    <div class="container">
        <div class="container pt-2">
            <a href="{{url('role/add')}}" class="btn btn-primary btn-lg active" role="button" aria-pressed="true">ADD</a>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="table-resposive">
                    <table class="table table-striped table-dark">
                        <thead>
                          <tr>
                          
                            <th scope="col">First</th>
                            <th scope="col">Last</th>
                            <th scope="col">Handle</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                        
                            <td>Mark</td>
                            <td>Otto</td>
                            <td>@mdo</td>
                          </tr>
                 
                         
                        </tbody>
                      </table>
                </div>
            </div>
        </div>
    </div>
</section>
    
@endsection