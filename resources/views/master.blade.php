<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>CRUD</title>

   <!-- CSRF Token -->
   <meta name="csrf-token" content="{{ csrf_token() }}">
        <!-- Scripts -->
        <script src="{{ asset('public/js/app.js') }}" defer></script>

        <!-- Styles -->
        <link href="{{ asset('public/css/app.css') }}" rel="stylesheet">


        {{-- <link rel="styesheet" href="{{asset('public/css/app.css')}}">
<script src="{{asset('public/js/app.js')}}"></script> --}}

</head>
<body>
    @include('include.header')
    @yield('content')
    @include('include.footer')
</body>
</html>